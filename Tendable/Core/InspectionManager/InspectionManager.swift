//
//  InspectionManager.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import Foundation

protocol InspectionManagerProtocol {
    func getInspection(complition: @escaping ((Result<Inspections, Error>) -> Void))
    func sendInspection(request: Inspections, complition: @escaping ((Result<Void, Error>) -> Void))
}

final class InspectionManager: InspectionManagerProtocol {
    private let networkClient: NetworkClientProtocol
    
    init(networkClient: NetworkClientProtocol) {
        self.networkClient = networkClient
    }
    
    func getInspection(complition: @escaping ((Result<Inspections, Error>) -> Void)) {
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.getInspections)
        self.networkClient.callAPI(request: request) { result in
            DispatchQueue.main.async {
                complition(result)
            }
        }
    }
    
    func sendInspection(request: Inspections, complition: @escaping ((Result<Void, Error>) -> Void)) {
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.sendInspections(inspection: request))
        self.networkClient.callAPIWithoutResponse(request: request) { result in
            DispatchQueue.main.async {
                complition(result)
            }
        }
    }
}
