//
//  DependencyFactory.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation
import UIKit

protocol Factory {
    var dependecies: Dependencies { get }
    
    func makeLogInViewController(coordinator: ProjectCoordinator) -> LogInViewController
    func makeSignUpViewController(coordinator: ProjectCoordinator) -> SignUpViewController
    func makeListOfInspectionsViewController(coordinator: ProjectCoordinator) -> ListOfInspectionsViewController
    func makeInspectionQuestionsViewController(coordinator: ProjectCoordinator, inspections: Inspections) ->
    InspectionQuestionsViewController
}

final class DependencyFactory: Factory {

    // MARK: Factory methods
    func makeLogInViewController(coordinator: ProjectCoordinator) -> LogInViewController {
        let authenticationManager: AuthenticationManagerProtocol = dependecies.authenticationManager
        let userDefaults: UserDefaultsManagerProtocol = dependecies.userDefaultsManager
        let viewController = LogInViewController()
        let presenter = LogInPresenter(coordinator: coordinator,
                                       view: viewController,
                                       authenticationManager: authenticationManager,
                                       userDefaultsManager: userDefaults)
        viewController.set(presenter: presenter)
        return viewController
    }
    
    func makeSignUpViewController(coordinator: ProjectCoordinator) -> SignUpViewController {
        let authenticationManager: AuthenticationManagerProtocol = dependecies.authenticationManager
        let viewController = SignUpViewController()
        let presenter = SignUpPresenter(coordinator: coordinator,
                                        view: viewController,
                                        authenticationManager: authenticationManager)
        viewController.set(presenter: presenter)
        return viewController
    }
    
    func makeListOfInspectionsViewController(coordinator: ProjectCoordinator) -> ListOfInspectionsViewController {
        let inspectionManager: InspectionManagerProtocol = dependecies.inspectionManager
        let realmManager = dependecies.realmManager
        let userDefaults: UserDefaultsManagerProtocol = dependecies.userDefaultsManager
        let viewController = ListOfInspectionsViewController()
        let presenter = ListOfInspectionsPresenter(coordinator: coordinator,
                                                   view: viewController,
                                                   inspectionManager: inspectionManager,
                                                   userDefaultsManager: userDefaults,
                                                   realmManager: realmManager)
        viewController.set(presenter: presenter)
        return viewController
    }
    
    func makeInspectionQuestionsViewController(coordinator: ProjectCoordinator, inspections: Inspections) ->
    InspectionQuestionsViewController {
        let realmManager = dependecies.realmManager
        let viewController = InspectionQuestionsViewController()
        let presenter = InspectionQuestionsPresenter(coordinator: coordinator, view: viewController, realmManager: realmManager)
        viewController.set(presenter: presenter, inspections: inspections)
        return viewController
    }

    // MARK: Dependencies
    var dependecies: Dependencies

    init(dependecies: Dependencies) {
        self.dependecies = dependecies
    }

    // MARK: Make Initial coordinator
    func makeInitialCoordinator() -> ProjectCoordinator {
        let coordinator = ProjectCoordinator(factory: self)
        return coordinator
    }
}
