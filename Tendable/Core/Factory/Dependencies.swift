//
//  Dependency.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation
import RealmSwift

final class Dependencies {
    lazy var networkClient: NetworkClientProtocol = NetworkClient()
    lazy var inspectionManager: InspectionManagerProtocol = InspectionManager(networkClient: networkClient)
    lazy var authenticationManager: AuthenticationManagerProtocol = AuthenticationManager(networkClient: networkClient)
    lazy var realmManager: RealmManagerProtocol = RealmManager(realm: realmClient)
    lazy var userDefaultsManager: UserDefaultsManagerProtocol = UserDefaultsManager()
    
    private lazy var realmClient: Realm? = try? Realm()
}

