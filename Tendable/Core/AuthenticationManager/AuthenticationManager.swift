//
//  InspectionManager.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import Foundation

protocol AuthenticationManagerProtocol {
    func login(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void))
    func registration(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void))
}

final class AuthenticationManager: AuthenticationManagerProtocol {
    private let networkClient: NetworkClientProtocol
    
    init(networkClient: NetworkClientProtocol) {
        self.networkClient = networkClient
    }
    
    func login(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void)) {
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.login(email: email, password: password))
        self.networkClient.callAPIWithoutResponse(request: request) { result in
            DispatchQueue.main.async {
                complition(result)
            }
        }
    }
    
    func registration(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void)) {
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.register(email: email, password: password))
        self.networkClient.callAPIWithoutResponse(request: request) { result in
            DispatchQueue.main.async {
                complition(result)
            }
        }
    }
}
