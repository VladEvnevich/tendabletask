//
//  ProjectCoordinator.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import UIKit

final class ProjectCoordinator: RootCoordinator, AbstractCoordinator {
    
    // MARK: Properties
    private(set) var childCoordinators: [AbstractCoordinator] = []
    weak var navigationContoller: UINavigationController?
    private var factory: Factory

    // MARK: Initialization
    init(factory: Factory) {
        self.factory = factory
    }

    // MARK: AbstractCoordinator methods
    func addChildCoordinator(_ coordinator: AbstractCoordinator) {
        childCoordinators.append(coordinator)
    }

    func removeAllChildCoordinatorsWith<T>(type: T.Type) {
        childCoordinators = childCoordinators.filter { $0 is T == false }
    }

    func removeAllChildCoordinators() {
        childCoordinators.removeAll()
    }

    // MARK: directly coordinator methods
    func start(_ navigationController: UINavigationController) {
        let viewController: UIViewController?
        if factory.dependecies.userDefaultsManager.isLoggedStored {
            viewController = factory.makeListOfInspectionsViewController(coordinator: self)
        } else {
            viewController = factory.makeSignUpViewController(coordinator: self)
        }
        
        self.navigationContoller = navigationController
        navigationController.pushViewController(viewController ?? UIViewController(), animated: true)
    }
    
    func moveToSignUp() {
        guard let hasSignUp = navigationContoller?.viewControllers.contains(where: { $0.isKind(of: SignUpViewController.self) }) else {
            navigationContoller?.viewControllers.removeAll()
            navigationContoller?.pushViewController(factory.makeSignUpViewController(coordinator: self), animated: true)
            return
        }
        if hasSignUp {
            navigationContoller?.popToRootViewController(animated: true)
        } else {
            navigationContoller?.viewControllers.removeAll()
            navigationContoller?.pushViewController(factory.makeSignUpViewController(coordinator: self), animated: true)
        }
    }
    
    func moveToLogIn() {
        let viewController = factory.makeLogInViewController(coordinator: self)
        navigationContoller?.pushViewController(viewController, animated: true)
    }
    
    func moveToListOfInspections() {
        let viewController = factory.makeListOfInspectionsViewController(coordinator: self)
        navigationContoller?.pushViewController(viewController, animated: true)
    }
    
    func moveToInspectionQuestions(with inspections: Inspections) {
        let viewController = factory.makeInspectionQuestionsViewController(coordinator: self, inspections: inspections)
        navigationContoller?.pushViewController(viewController, animated: true)
    }
}
