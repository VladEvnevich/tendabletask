//
//  RootCoordinator.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import UIKit

/// Root Coordinator
protocol RootCoordinator: AnyObject {
    func start(_ navigationController: UINavigationController)
    func moveToLogIn()
    func moveToListOfInspections()
    func moveToInspectionQuestions(with inspections: Inspections)
    func moveToSignUp()
}
