//
//  AbstractCoordinator.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

protocol AbstractCoordinator {
    func addChildCoordinator(_ coordinator: AbstractCoordinator)
    func removeAllChildCoordinatorsWith<T>(type: T.Type)
    func removeAllChildCoordinators()
}
