//
//  UserDefaultsManager.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import Foundation

protocol UserDefaultsManagerProtocol {
    var isLoggedStored: Bool { get set }
}

final class UserDefaultsManager: UserDefaultsManagerProtocol {
    private let isLogged = "isLogged"
    private let userDefaults: UserDefaults
    
    init() {
        userDefaults = UserDefaults.standard
    }
    
    var isLoggedStored: Bool {
        get {
            return userDefaults.bool(forKey: isLogged)
        }
        set {
            userDefaults.set(newValue, forKey: isLogged)
        }
    }
}
