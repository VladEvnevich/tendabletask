//
//  RealmManager.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import Foundation
import RealmSwift

protocol RealmManagerProtocol {
    func saveInspection(inspection: Inspections)
    func getCurrentInspection() -> Inspections?
    func removeInspection()
}

class RealmManager: RealmManagerProtocol {
    private let realm: Realm?
    
    init(realm: Realm?) {
        self.realm = realm
    }
    
    func getCurrentInspection() -> Inspections? {
        guard let inspections = realm?.object(ofType: DBInspections.self, forPrimaryKey: DBInspections.uniqueKey) else { return nil }
        return Inspections(dbInspections: inspections)
        
    }
    
    func saveInspection(inspection: Inspections) {
        try? realm?.write {
            let dbInspection = DBInspections(model: inspection)
            realm?.add(dbInspection, update: .modified)
        }
    }
    
    func removeInspection() {
        try? realm?.write({
            realm?.deleteAll()
        })
    }
}
