//
//  DBInspectionsModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBInspections: Object {
    static let uniqueKey = "DBInspections"
    
    @Persisted var uniqueKey: String = DBInspections.uniqueKey
    @Persisted var inspection: DBInspection?
    
    
    override static func primaryKey() -> String? {
        return "uniqueKey"
    }
    
    convenience init(model: Inspections) {
        self.init()
        let inspection = DBInspection(model: model.inspection)
        self.inspection = inspection
    }
}

extension Inspections {
    init(dbInspections: DBInspections) {
        self.inspection = Inspection(dbInspection: dbInspections.inspection ?? DBInspection())
    }
}
