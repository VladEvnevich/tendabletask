//
//  DBInspectionType.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBInspectionType: Object {
    @Persisted var id: Int
    @Persisted var name: String
    @Persisted var access: Access.RawValue
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: InspectionType) {
        self.init()
        self.id = model.id
        self.name = model.name
        self.access = model.access.rawValue
    }
}

extension InspectionType {
    init(dbInspectionType: DBInspectionType) {
        self.id = dbInspectionType.id
        self.name = dbInspectionType.name
        self.access = Access(rawValue: dbInspectionType.access) ?? .none
    }
}
