//
//  DBQuestionModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBQuestion: Object {
    @Persisted var id: Int
    @Persisted var name: String
    @Persisted var answerChoices: List<DBAnswerChoice>
    @Persisted var selectedAnserChoiceId: Int?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: Question) {
        self.init()
        self.id = model.id
        self.name = model.name
        self.selectedAnserChoiceId = model.selectedAnserChoiceId
        let answerChoices = model.answerChoices.map { choice in
            return DBAnswerChoice(model: choice)
        }
        self.answerChoices.append(objectsIn: answerChoices)
    }
}

extension Question {
    init(dbQuestion: DBQuestion) {
        self.id = dbQuestion.id
        self.name = dbQuestion.name
        self.selectedAnserChoiceId = dbQuestion.selectedAnserChoiceId
        let answerChoices: [AnswerChoice] = dbQuestion.answerChoices.map { choise in
            return AnswerChoice(dbAnswerChoice: choise)
        }
        self.answerChoices = answerChoices
    }
}
