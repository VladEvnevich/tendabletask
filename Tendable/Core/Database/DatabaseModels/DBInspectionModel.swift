//
//  DBInspectionModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBInspection: Object {
    @Persisted var id: Int
    @Persisted var inspectionType: DBInspectionType?
    @Persisted var area: DBArea?
    @Persisted var survey: DBSurvey?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: Inspection) {
        self.init()
        let inspectionType = DBInspectionType(model: model.inspectionType)
        let area = DBArea(model: model.area)
        let survey = DBSurvey(model: model.survey)
        self.id = model.id
        self.inspectionType = inspectionType
        self.area = area
        self.survey = survey
    }
}

extension Inspection {
    init(dbInspection: DBInspection) {
        self.id = dbInspection.id
        self.area = Area(dbArea: dbInspection.area ?? DBArea())
        self.survey = Survey(dbSurvey: dbInspection.survey ?? DBSurvey())
        self.inspectionType = InspectionType(dbInspectionType: dbInspection.inspectionType ?? DBInspectionType())
    }
}
