//
//  DBSurveyModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBSurvey: Object {
    @Persisted var id: Int
    @Persisted var categories: List<DBCategory>
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: Survey) {
        self.init()
        self.id = model.id
        let categories = model.categories.map { category in
            return DBCategory(model: category)
        }
        self.categories.append(objectsIn: categories)
    }
}

extension Survey {
    init(dbSurvey: DBSurvey) {
        self.id = dbSurvey.id
        let categories: [Category] = dbSurvey.categories.map { category in
            return Category(dbCategory: category)
        }
        self.categories = categories
    }
}
