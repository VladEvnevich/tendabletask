//
//  DBArea.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBArea: Object {
    @Persisted var id: Int
    @Persisted var name: String
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: Area) {
        self.init()
        self.id = model.id
        self.name = model.name
    }
}

extension Area {
    init(dbArea: DBArea) {
        self.id = dbArea.id
        self.name = dbArea.name
    }
}
