//
//  DBAnswerChoiceModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBAnswerChoice: Object {
    @Persisted var id: Int
    @Persisted var name: String
    @Persisted var score: Double
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: AnswerChoice) {
        self.init()
        self.id = model.id
        self.name = model.name
        self.score = model.score
    }
}

extension AnswerChoice {
    init(dbAnswerChoice: DBAnswerChoice) {
        self.id = dbAnswerChoice.id
        self.name = dbAnswerChoice.name
        self.score = dbAnswerChoice.score
    }
}
