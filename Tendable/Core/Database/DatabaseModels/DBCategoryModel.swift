//
//  DBCategoryModel.swift
//  Tendable
//
//  Created by user on 13.04.2022.
//

import RealmSwift
import Foundation

class DBCategory: Object {
    @Persisted var id: Int
    @Persisted var name: String
    @Persisted var questions: List<DBQuestion>
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(model: Category) {
        self.init()
        self.id = model.id
        self.name = model.name
        let questions = model.questions.map { question in
            return DBQuestion(model: question)
        }
        self.questions.append(objectsIn: questions)
    }
}

extension Category {
    init(dbCategory: DBCategory) {
        self.id = dbCategory.id
        self.name = dbCategory.name
        let questions: [Question] = dbCategory.questions.map { question in
            return Question(dbQuestion: question)
        }
        self.questions = questions
    }
}
