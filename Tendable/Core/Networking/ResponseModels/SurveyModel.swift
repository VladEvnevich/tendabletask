//
//  SurveyModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

struct Survey: Codable {
    let id: Int
    var categories: [Category]
}
