//
//  User.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import Foundation

struct User: Codable {
    let email: String
    let password: String
}
