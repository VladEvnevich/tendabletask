//
//  InspectionModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

struct Inspection: Codable {
    let id: Int
    var inspectionType: InspectionType
    let area: Area
    var survey: Survey
}
