//
//  InspectionsModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import Foundation

struct Inspections: Codable {
    var inspection: Inspection
}
