//
//  AreaModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

struct Area: Codable {
    let id: Int
    let name: String
}
