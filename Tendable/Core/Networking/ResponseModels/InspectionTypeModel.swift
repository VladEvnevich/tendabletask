//
//  InspectionTypeModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

// MARK: - Inspection Type Model
struct InspectionType: Codable {
    let id: Int
    var name: String
    let access: Access
}

enum Access: String, Codable {
    case none
    case read
    case write
}
