//
//  QuestionModel.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

struct Question: Codable {
    let id: Int
    let name: String
    var answerChoices: [AnswerChoice]
    var selectedAnserChoiceId: Int?
}
