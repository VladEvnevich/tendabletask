//
//  NetworkClient.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

protocol NetworkClientProtocol {
    func callAPI<T: Decodable>(request: URLRequest, completion: @escaping (Result<T, Error>) -> Void)
    func callAPIWithoutResponse(request: URLRequest, completion: @escaping (Result<Void, Error>) -> Void)
}

final class NetworkClient: NetworkClientProtocol {
    
    // MARK: - Properties
    private lazy var jsonDecoder = JSONDecoder()
    private var urlSession: URLSessionProtocol
    
    // MARK: - Initialization
    init(session: URLSessionProtocol = URLSession(configuration: URLSessionConfiguration.default)) {
        self.urlSession = session
    }
    
    // MARK: - API call with response
    func callAPI<T: Decodable>(request: URLRequest, completion: @escaping (Result<T, Error>) -> Void) {
        
        let task = self.urlSession.dataTask(with: request) { (data, response, _) in
            guard response != nil else {
                completion(.failure(NetworkError.connectionError))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                let statusCode = httpResponse.statusCode
                
                switch statusCode {
                case 500...599:
                    completion(.failure(NetworkError.serverError))
                case 400...499:
                    completion(.failure(NetworkError.clientError))
                case 300...399:
                    completion(.failure(NetworkError.redirectionError))
                case 100...199:
                    completion(.failure(NetworkError.redirectionError))
                default:
                    do {
                        guard let data = data else { completion(.failure(NetworkError.emptyData))
                            return
                        }
                        let object = try self.jsonDecoder.decode(T.self, from: data)
                        completion(.success(object))
                    } catch {
                        completion(.failure(NetworkError.jsonParsingFailure))
                    }
                }
            }
        }
        task.resume()
    }
    
    // MARK: - API call without response
    func callAPIWithoutResponse(request: URLRequest, completion: @escaping (Result<Void, Error>) -> Void) {
        
        let task = self.urlSession.dataTask(with: request) { (data, response, _) in
            guard response != nil else {
                completion(.failure(NetworkError.connectionError))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                let statusCode = httpResponse.statusCode
                
                switch statusCode {
                case 500...599:
                    completion(.failure(NetworkError.serverError))
                case 400...499:
                    completion(.failure(NetworkError.clientError))
                case 300...399:
                    completion(.failure(NetworkError.redirectionError))
                case 100...199:
                    completion(.failure(NetworkError.redirectionError))
                case 200...299:
                    completion(.success(()))
                default:
                    completion(.failure(NetworkError.serverError))
                }
            }
        }
        task.resume()
    }
}
