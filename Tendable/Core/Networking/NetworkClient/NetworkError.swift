//
//  NetworkError.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

enum NetworkError: Error {
    case encodingFailed
    case missingURL
    case emptyData
    case jsonParsingFailure
    case serverError
    case clientError
    case redirectionError
    case connectionError
    case informationalError

    var localizedDescription: String {
        switch self {
        case .encodingFailed:
            return "encodingFailed"
        case .missingURL:
            return "missingURL"
        case .emptyData:
            return "emptyData"
        case .jsonParsingFailure:
            return "jsonParsingFailure"
        case .serverError:
            return "serverError"
        case .clientError:
            return "clientError"
        case .connectionError:
            return "connectionError"
        case .redirectionError:
            return "redirectionError"
        case .informationalError:
            return "informationalError"
        }
    }
}
