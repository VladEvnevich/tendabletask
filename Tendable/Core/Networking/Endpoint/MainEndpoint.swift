//
//  MainEndpoint.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

enum MainEndpoint: EndPointProtocol {
    case register(email: String, password: String)
    case login(email: String, password: String)
    case getInspections
    case sendInspections(inspection: Inspections)
    case getRandomInspections(count: Int)
    case getRandomInspection(id: Int?)
    case deleteInspections(id: Int)

    // MARK: - Computed Properties
    var baseURL: URL? {
        URL(string: "Http://127.0.0.1:5001")
    }

    var path: String {
        switch self {
        case .register:
            return "/api/register"
        case .login:
            return "/api/login"
        case .getInspections:
            return "/api/inspections/start"
        case .sendInspections:
            return "/api/inspections/submit"
        case .getRandomInspections(let count):
            return "/api/generate_random_inspections/\(count)"
        case .getRandomInspection(let id):
            return "/api/random_inspection\(id != nil ? "/\(id!)" : "")"
        case .deleteInspections(let id):
            return "/api/inspections/\(id)"
        }
    }
    var httpMethod: HTTPMethod {
        switch self {
        case .register,
                .login,
                .sendInspections:
            return .post
        case .deleteInspections:
            return .delete
        default:
            return .get
        }
    }

    var task: HTTPTask {
        switch self {
        case .register(let email, let password),
                .login(let email, let password):
            let parameters = [
                "email": email,
                "password": password
            ]
            return .requestParameters(bodyParameters: parameters, urlParameters: nil)
        case .getInspections,
                .getRandomInspections,
                .getRandomInspection,
                .deleteInspections:
            return .request
        case .sendInspections(let inspections):
            do {
                let json = try inspections.asDictionary()
                return .requestParameters(bodyParameters: json, urlParameters: nil)
            } catch {
                print("error")
            }
            return .request
            
        }
    }

    var headers: HTTPHeaders {
        return ["": ""]
    }
}

