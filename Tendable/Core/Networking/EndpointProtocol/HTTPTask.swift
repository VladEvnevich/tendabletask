//
//  HTTPTask.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import Foundation

typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case request
    case requestParameters(bodyParameters: Parameters?,
                           urlParameters: Parameters?)
    case requestParametersAndHeaders(
            bodyParameters: Parameters?,
            urlParameters: Parameters?,
            additionHeaders: HTTPHeaders
         )
}
