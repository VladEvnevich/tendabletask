//
//  RadioButton.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit
import SnapKit

protocol RadioButtonDelegate: AnyObject {
    func buttonDidTap(with id: Int)
}

class RadioButton: UIView {
    
    // MARK: - Properties
    var isSelected: Bool = false {
        didSet {
            updateUI()
        }
    }
    
    weak var delegate: RadioButtonDelegate?
    let id: Int
    
    // MARK: - GUI variables
    private lazy var button: UIView = {
        let item = UIView()
        return item
    }()
    
    private lazy var label: UILabel = {
        let item = UILabel()
        item.numberOfLines = 0
        item.contentMode = .top
        return item
    }()
    
    // MARK: - Initialization
    init(selected: Bool, text: String, id: Int) {
        self.id = id
        super.init(frame: .zero)
        self.isSelected = selected
        self.label.text = text
        
        configureUI()
        configureLogic()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        button.layer.cornerRadius = button.bounds.height/2
    }
    
    // MARK: - Private methods
    private func configureUI() {
        addSubview(button)
        addSubview(label)
        updateUI()
        
        button.snp.makeConstraints {
            $0.left.equalToSuperview().inset(10)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(15)
        }
        label.snp.makeConstraints {
            $0.top.equalTo(button)
            $0.firstBaseline.equalTo(button.snp.firstBaseline)
            $0.left.equalTo(button.snp.right).offset(20)
            $0.right.equalToSuperview().inset(10)
            $0.bottom.equalToSuperview().inset(10)
        }
    }
    
    private func configureLogic() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.toggleSelection))
        self.addGestureRecognizer(tapGesture)
    }
    
    private func updateUI() {
        button.backgroundColor = isSelected ? UIColor(named: "borderColor") : .black
    }
    
    // MARK: - Actions
    @objc func toggleSelection() {
        isSelected.toggle()
        delegate?.buttonDidTap(with: self.id)
    }
}
