//
//  CustomTextField.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit
import SnapKit

class CustomTextField: UITextField {
    // MARK: - Properties
    var placeholderColor: UIColor = UIColor.lightGray {
        didSet {
            self.setPlaceholderColor()
        }
    }
    
    var textPadding = UIEdgeInsets(
        top: 10,
        left: 20,
        bottom: 10,
        right: 20
    )
    
    private lazy var rightButton: UIButton = {
        let button = UIButton()
        button.tintColor = UIColor(named: "placeholderColor") ?? UIColor()
        button.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        button.addTarget(self, action: #selector(rightButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override var isSecureTextEntry: Bool {
        didSet {
            updateButtonImage()
        }
    }
    
    // MARK: - Initialization
    init(placeholder: String, secure: Bool) {
        super.init(frame: .zero)

        if secure {
            rightViewMode = .always
            rightButton.setImage(
                UIImage(systemName: "eye.slash.fill"),
                for: .normal)
            rightView = rightButton
            isSecureTextEntry = true
        }
        
        self.placeholderColor = UIColor(named: "placeholderColor") ?? UIColor()
        self.placeholder = placeholder
        self.textColor = UIColor(named: "blackTextColor") ?? UIColor()
        self.backgroundColor = .white
        self.layer.cornerRadius = 20
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private methods
    private func setPlaceholderColor() {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : placeholderColor])
    }
    
    private func updateButtonImage() {
        if isSecureTextEntry {
            rightButton.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        } else {
            rightButton.setImage(UIImage(systemName: "eye"), for: .normal)
        }
    }
    
    // MARK: - Override methods
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width-45, y: 15, width: 45, height: bounds.height/2)
    }
    
    // MARK: Actions
    @objc func rightButtonPressed() {
        isSecureTextEntry.toggle()
    }
}
