//
//  InspectionQuestionCell.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit
import SnapKit

protocol InspectionsQuestionCellDelegate: AnyObject {
    func answerTapped(isEmpty: Bool, with id: Int, from section: Int, from id: Int)
}

class InspectionsQuestionCell: UITableViewCell {
    
    // MARK: - Properties
    weak var answerDelegate: InspectionsQuestionCellDelegate?
    
    // MARK: - GUI variables
    let backgroundCellView = UIView()
    let borderView = UIView()
    var questions = [AnswerChoice]()
    var buttons: [RadioButton] = []
    var section: Int?
    var row: Int?
    
    lazy var questionLabel: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.boldSystemFont(ofSize: 17)
        item.numberOfLines = 0
        return item
    }()
    
    lazy var mainStack: UIStackView = {
        let item = UIStackView(arrangedSubviews: [])
        item.axis = .vertical
        item.alignment = .fill
        item.distribution = .fill
        return item
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private methods
    private func configureLayout() {
        contentView.addSubview(backgroundCellView)
        backgroundCellView.addSubview(borderView)
        borderView.addSubview(questionLabel)
        borderView.addSubview(mainStack)
        
        backgroundCellView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(5)
            $0.left.right.equalToSuperview()
        }
        borderView.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(10)
            $0.top.bottom.equalToSuperview().inset(10)
        }
        questionLabel.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(10)
            $0.top.equalToSuperview().inset(10)
        }
        mainStack.snp.makeConstraints {
            $0.top.equalTo(questionLabel.snp.bottom).offset(10)
            $0.left.right.bottom.equalToSuperview().inset(10)
        }
    }
    
    private func configureUI() {
        backgroundColor = UIColor(named: "backgroundColor")
        borderView.backgroundColor = .white
        borderView.layer.cornerRadius = 20
        borderView.layer.borderColor = UIColor(named: "borderColor")?.cgColor
        borderView.layer.borderWidth = 2.0
        mainStack.backgroundColor = .white
    }
    
    // MARK: - Configuration
    func configureCell(question: String, answers: [AnswerChoice], selectedId: Int?, section: Int, row: Int) {
        self.questionLabel.text = question
        self.questions = answers
        self.section = section
        self.row = row
        
        buttons.forEach({$0.removeFromSuperview()})
        buttons = []
        answers.forEach({
            let radionButton = RadioButton(selected: false, text: $0.name, id: $0.id)
            radionButton.delegate = self
            buttons.append(radionButton)
            mainStack.addArrangedSubview(radionButton)
        })
        
        buttons.first(where: {$0.id == selectedId})?.isSelected = true
        
        configureLayout()
        configureUI()
    }
}

extension InspectionsQuestionCell {
    static var reuseIdentifier: String {
        return "InspectionsQuestionCell"
    }
}

extension InspectionsQuestionCell: RadioButtonDelegate {
    func buttonDidTap(with id: Int) {
        self.buttons.map({if $0.id != id {$0.isSelected = false}})
        guard let section = section, let row = row else { return }
        
        if !buttons.contains(where: {$0.isSelected}) {
            answerDelegate?.answerTapped(isEmpty: true, with: id, from: section, from: row)
            return 
        }
        answerDelegate?.answerTapped(isEmpty: false, with: id, from: section, from: row)
    }
}
