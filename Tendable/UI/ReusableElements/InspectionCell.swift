//
//  InspectionCell.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit
import SnapKit

protocol CustomTableViewCellDelegate: AnyObject {
    func continueDidTap()
}

class CustomTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    weak var buttonDelegate: CustomTableViewCellDelegate?
    
    // MARK: - GUI variables
    let backgroundCellView = UIView()
    let borderView = UIView()
    
    lazy var typeLabel: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.boldSystemFont(ofSize: 17)
        item.text = "Type:"
        return item
    }()
    
    lazy var areaLabel: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.boldSystemFont(ofSize: 17)
        item.text = "Area:"
        return item
    }()
    
    lazy var accessLabel: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.boldSystemFont(ofSize: 17)
        item.text = "Access:"
        return item
    }()
    
    lazy var typePlaceholder: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.systemFont(ofSize: 16)
        return item
    }()
    
    lazy var areaPlaceholder: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.systemFont(ofSize: 16)
        return item
    }()
    
    lazy var accessPlaceholder: UILabel = {
        let item = UILabel()
        item.contentMode = .left
        item.font = UIFont.systemFont(ofSize: 16)
        return item
    }()
    
    lazy var continueButton: UIButton = {
        let item = UIButton()
        item.setTitle("Continue", for: .normal)
        item.setTitleColor(.black, for: .normal)
        item.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        item.addTarget(self, action: #selector(continueButtonDidTap), for: .touchUpInside)
        return item
    }()
    
    lazy var leftStack: UIStackView = {
        let item = UIStackView(arrangedSubviews: [
            typeLabel,
            areaLabel,
            accessLabel])
        item.axis = .vertical
        item.alignment = .leading
        item.distribution = .fillProportionally
        return item
    }()
    
    lazy var middleStack: UIStackView = {
        let item = UIStackView(arrangedSubviews: [
            typePlaceholder,
            areaPlaceholder,
            accessPlaceholder
        ])
        item.axis = .vertical
        item.alignment = .fill
        item.distribution = .fillEqually //.fillProportionally
        return item
    }()
    
    lazy var mainStack: UIStackView = {
        let item = UIStackView(arrangedSubviews: [
            leftStack,
            middleStack,
            continueButton
        ])
        item.axis = .horizontal
        item.alignment = .bottom
        item.distribution = .fillProportionally
        item.isUserInteractionEnabled = true
        return item
    }()

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private methods
    private func configureLayout() {
        contentView.addSubview(backgroundCellView)
        backgroundCellView.addSubview(borderView)
        borderView.addSubview(mainStack)
        
        backgroundCellView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(5)
            $0.left.right.equalToSuperview()
        }
        borderView.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(10)
            $0.top.bottom.equalToSuperview().inset(10)
        }
        mainStack.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(10)
        }
    }
    
    private func configureUI() {
        backgroundColor = UIColor(named: "backgroundColor")
        borderView.backgroundColor = .white
        borderView.layer.cornerRadius = 20
        borderView.layer.borderColor = UIColor(named: "borderColor")?.cgColor
        borderView.layer.borderWidth = 2.0
        mainStack.backgroundColor = .white
    }
    
    // MARK: - Configuration
    func configureCell(type: String, area: String, access: String) {
        self.typePlaceholder.text = type
        self.areaPlaceholder.text = area
        self.accessPlaceholder.text = access
    }
    
    // MARK: - Actions
    @objc func continueButtonDidTap() {
        buttonDelegate?.continueDidTap()
    }
}

extension CustomTableViewCell {
    static var reuseIdentifier: String {
        return "CustomTableViewCell"
    }
}
