//
//  CustomButton.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit

protocol CustomButtonDelegate: AnyObject {
    func customButtonPressed(with id: Int)
}

final class CustomButton: UIButton {

    // MARK: Properties
    weak var delegate: CustomButtonDelegate?
    private var id: Int

    // MARK: Initialization
    init(text: String, id: Int) {
        self.id = id
        super.init(frame: .zero)
        configureButton(with: text)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }

    // MARK: Methods

    private func configureButton(with text: String) {
        setTitleColor(UIColor.white, for: .normal)
        setTitle(text, for: .normal)
        backgroundColor = UIColor(named: "buttonColor")
        titleLabel?.font = UIFont.systemFont(ofSize: 32)
        addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }

    // MARK: Actions
    @objc func buttonPressed() {
        delegate?.customButtonPressed(with: self.id)
    }
}
