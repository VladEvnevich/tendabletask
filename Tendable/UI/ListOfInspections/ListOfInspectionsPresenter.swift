//
//  ListOfInspectionsPresenter.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import UIKit

protocol ListOfInspectionsPresenterProtocol {
    func fetchLocalInspections()
    func fetchRemoteInspections()
    func continueButtonDidTap()
    func logoutButtonDidTap()
    func sendInspectations()
}

final class ListOfInspectionsPresenter {

    // MARK: Properties
    weak private var view: ListOfInspectionsViewControllerProtocol?
    private var coordinator: RootCoordinator?
    private var inspectionManager: InspectionManagerProtocol?
    private var userDefaultsManager: UserDefaultsManagerProtocol?
    private var realmManager: RealmManagerProtocol

    // MARK: Initialization
    init(coordinator: RootCoordinator,
         view: ListOfInspectionsViewControllerProtocol,
         inspectionManager: InspectionManagerProtocol,
         userDefaultsManager: UserDefaultsManagerProtocol,
         realmManager: RealmManagerProtocol) {
        self.coordinator = coordinator
        self.view = view
        self.inspectionManager = inspectionManager
        self.userDefaultsManager = userDefaultsManager
        self.realmManager = realmManager
    }
    
    // MARK: - Private methods
    private func isInspectionsComplete(_ inspections: Inspections) -> Bool {
        !inspections.inspection.survey.categories.contains(where: {$0.questions.contains(where: {$0.selectedAnserChoiceId == nil})})
    }
}

// MARK: ListOfInspectionsPresenterProtocol
extension ListOfInspectionsPresenter: ListOfInspectionsPresenterProtocol {
    func logoutButtonDidTap() {
        userDefaultsManager?.isLoggedStored = false
        coordinator?.moveToSignUp()
    }
    
    func sendInspectations() {
        guard let inspectations = view?.inspections else { return }
        guard isInspectionsComplete(inspectations) else {
            view?.showAlert(title: "Error", message: "Please answer all questions", style: .destructive)
            return
        }
        
        inspectionManager?.sendInspection(request: inspectations, complition: { [weak self] result in
            switch result {
            case .failure(let error):
                self?.view?.showAlert(title: "Error", message: error.localizedDescription, style: .destructive)
            case .success():
                DispatchQueue.main.async {
                    self?.realmManager.removeInspection()
                    self?.view?.showInitialState()
                    self?.view?.showAlert(title: "Done", message: "Submitting completed with success", style: .default)
                }
            }
        })
    }
    
    func continueButtonDidTap() {
        guard let inspections = view?.inspections else { return }
        coordinator?.moveToInspectionQuestions(with: inspections)
    }
    
    func fetchRemoteInspections() {
        inspectionManager?.getInspection(complition: { [weak self] result in
            switch result {
            case .success(let inspections):
                self?.view?.showInspections(inspections)
            case .failure(let error):
                self?.view?.showAlert(title: "Error", message: error.localizedDescription, style: .destructive)
            }
        })
    }
    
    func fetchLocalInspections() {
        guard let inspections = realmManager.getCurrentInspection() else {
            view?.showInitialState()
            return
        }
        view?.showInspections(inspections)
    }
}

