//
//  ListOfInspectionsViewController.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import UIKit
import SnapKit

protocol ListOfInspectionsViewControllerProtocol: AnyObject {
    func showAlert(title: String, message: String, style: UIAlertAction.Style)
    func showInspections(_ inspections: Inspections)
    func showInitialState()
    var inspections: Inspections? { get }
}

final class ListOfInspectionsViewController: UIViewController {
    
    // MARK: Properties
    var presenter: ListOfInspectionsPresenterProtocol?
    var inspections: Inspections?
    
    // MARK: - GUI variables
    private lazy var startButton: CustomButton = {
        let item = CustomButton(text: "Start", id: 0)
        item.delegate = self
        return item
    }()
    
    private var noInspectionsLabel: UILabel = {
        let item = UILabel()
        item.text = """
                    You currently have no active \n inspections \n \n
                    Press the start button to create a new \n inspection
                    """
        item.textAlignment = .center
        item.numberOfLines = 0
        item.font = UIFont.boldSystemFont(ofSize: 18)
        return item
    }()
    
    private lazy var tableView: UITableView = {
        let item = UITableView()
        item.backgroundColor = UIColor(named: "backgroundColor")
        item.showsVerticalScrollIndicator = false
        item.register(CustomTableViewCell.self, forCellReuseIdentifier: CustomTableViewCell.reuseIdentifier)
        item.separatorStyle = .none
        item.delegate = self
        item.dataSource = self
        return item
    }()
    
    private lazy var submitButton: CustomButton = {
        let item = CustomButton(text: "Submit", id: 1)
        item.delegate = self
        return item
    }()
    
    // MARK: - Initialization
    func set(presenter: ListOfInspectionsPresenterProtocol) {
        self.presenter = presenter
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: true)
        presenter?.fetchLocalInspections()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationItem.setHidesBackButton(false, animated: true)
    }
    
    // MARK: - Private methods
    private func configureUI() {
        view.backgroundColor = UIColor(named: "backgroundColor")
        title = "Tendable"
        
        let logoutButton: UIBarButtonItem = UIBarButtonItem(title: "Log out",
                                                           style: .done,
                                                           target:self,
                                                           action:#selector(logOutDidTap))
        self.navigationItem.setRightBarButton(logoutButton, animated:false)
    }
    
    private func configureInitialState() {
        tableView.removeFromSuperview()
        submitButton.removeFromSuperview()
        
        view.addSubview(noInspectionsLabel)
        view.addSubview(startButton)
        
        noInspectionsLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(130)
            $0.left.right.equalToSuperview().inset(37)
            $0.height.equalTo(164)
        }
        startButton.snp.makeConstraints {
            $0.top.equalTo(noInspectionsLabel.snp.bottom).offset(50)
            $0.left.right.equalToSuperview().inset(140)
            $0.height.equalTo(60)
        }
    }
    
    private func configureInspectionsSate() {
        noInspectionsLabel.removeFromSuperview()
        startButton.removeFromSuperview()
        
        view.addSubview(tableView)
        view.addSubview(submitButton)
        tableView.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(20)
            $0.top.equalToSuperview()
            $0.bottom.equalTo(submitButton.snp.top).inset(-20)
        }
        submitButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().inset(50)
            $0.left.right.equalToSuperview().inset(140)
            $0.height.equalTo(60)
        }
    }
    
    // MARK: - Actions
    @objc func logOutDidTap() {
        presenter?.logoutButtonDidTap()
    }
}

// MARK: - ListOfInspectionsViewControllerProtocol
extension ListOfInspectionsViewController: ListOfInspectionsViewControllerProtocol {
    func showAlert(title: String, message: String, style: UIAlertAction.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Понятно", style: style, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    func showInitialState() {
        configureInitialState()
    }
    func showInspections(_ inspections: Inspections) {
        self.inspections = inspections
        configureInspectionsSate()
    }
}

// MARK: - CustomButtonDelegate
extension ListOfInspectionsViewController: CustomButtonDelegate {
    func customButtonPressed(with id: Int) {
        switch id {
        case 0:
            presenter?.fetchRemoteInspections()
        default:
            presenter?.sendInspectations()
        }
        
    }
}

// MARK: - UITableViewDelegates
extension ListOfInspectionsViewController: UITableViewDelegate, UITableViewDataSource, CustomTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard inspections != nil else { return 0 }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.reuseIdentifier, for: indexPath) as? CustomTableViewCell else { return UITableViewCell() }
        guard let inspection = inspections?.inspection else { return cell }
        cell.configureCell(type: inspection.inspectionType.name,
                           area: inspection.area.name,
                           access: inspection.inspectionType.access.rawValue)
        cell.buttonDelegate = self
        return cell
    }
    
    func continueDidTap() {
        presenter?.continueButtonDidTap()
    }
}


