//
//  OnboardingPresenter.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import UIKit

protocol LogInPresenterProtocol {
    func logInButtonPressed(email: String?, password: String?)
}

final class LogInPresenter {

    // MARK: Properties

    weak private var view: LogInViewControllerProtocol?
    private var coordinator: RootCoordinator?
    private var authenticationManager: AuthenticationManagerProtocol?
    private var userDefaultsManager: UserDefaultsManagerProtocol?

    // MARK: Initialization

    init(coordinator: RootCoordinator,
         view: LogInViewControllerProtocol,
         authenticationManager: AuthenticationManagerProtocol,
         userDefaultsManager: UserDefaultsManagerProtocol) {
        self.coordinator = coordinator
        self.view = view
        self.authenticationManager = authenticationManager
        self.userDefaultsManager = userDefaultsManager
    }
}

// MARK: LogInPresenterProtocol
extension LogInPresenter: LogInPresenterProtocol {
    func logInButtonPressed(email: String?, password: String?) {
        guard let email = email, let password = password,
              !email.isEmpty, !password.isEmpty else {
                  view?.showAlert(with: "Fields is empty")
                  return
              }
        authenticationManager?.login(email: email, password: password, complition: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.view?.showAlert(with: error.localizedDescription)
            case .success():
                self.userDefaultsManager?.isLoggedStored = true
                self.coordinator?.moveToListOfInspections()
            }
        })
    }
}
