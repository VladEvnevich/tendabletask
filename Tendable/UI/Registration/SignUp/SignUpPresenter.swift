//
//  SignUpPresenter.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit

protocol SignUpPresenterProtocol {
    func signUpButtonPressed(email: String?, password: String?)
}

final class SignUpPresenter {

    // MARK: Properties
    weak private var view: SignUpViewControllerProtocol?
    private var coordinator: RootCoordinator?
    private var authenticationManager: AuthenticationManagerProtocol?

    // MARK: Initialization
    init(coordinator: RootCoordinator,
         view: SignUpViewControllerProtocol,
         authenticationManager: AuthenticationManagerProtocol) {
        self.coordinator = coordinator
        self.view = view
        self.authenticationManager = authenticationManager
    }
}

// MARK: SignUpPresenterProtocol
extension SignUpPresenter: SignUpPresenterProtocol {
    func signUpButtonPressed(email: String?, password: String?) {
        guard let email = email, let password = password,
        !email.isEmpty, !password.isEmpty else {
            view?.showAlert(with: "Fields is empty")
            return
        }
        authenticationManager?.registration(email: email, password: password, complition: { [weak self] result in
            switch result {
            case .failure(let error):
                self?.view?.showAlert(with: error.localizedDescription)
            case .success():
                self?.coordinator?.moveToLogIn()
            }
        })
    }
}
