//
//  SignUpViewController.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import UIKit
import SnapKit

protocol SignUpViewControllerProtocol: AnyObject {
    func showAlert(with message: String?)
}

final class SignUpViewController: UIViewController {
    
    // MARK: Properties
    var presenter: SignUpPresenterProtocol?

    // MARK: - GUI variables
    private let emailTextField = CustomTextField(placeholder: "Enter email", secure: false)
    private let passwordTextField = CustomTextField(placeholder: "Enter password", secure: true)
    private let signUpButton = CustomButton(text: "Sign Up", id: 4)
    
    private lazy var titleLabel: UILabel = {
        let item = UILabel()
        item.text = "Sign up"
        item.font = UIFont.boldSystemFont(ofSize: 40)
        item.textAlignment = .center
        return item
    }()
    
    // MARK: - Initialization
    func set(presenter: SignUpPresenterProtocol) {
        self.presenter = presenter
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_ :)))
        view.addGestureRecognizer(tapGesture)
        signUpButton.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cleanFields()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        view.backgroundColor = UIColor(named: "backgroundColor")
        view.addSubview(titleLabel)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(signUpButton)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(120)
            $0.left.right.equalToSuperview().inset(50)
        }
        emailTextField.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(150)
            $0.left.right.equalToSuperview().inset(35)
            $0.height.equalTo(60)
        }
        passwordTextField.snp.makeConstraints {
            $0.top.equalTo(emailTextField.snp.bottom).offset(35)
            $0.left.right.equalToSuperview().inset(35)
            $0.height.equalTo(60)
        }
        signUpButton.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(90)
            $0.top.equalTo(passwordTextField.snp.bottom).offset(65)
            $0.height.equalTo(80)
        }
    }
    
    private func cleanFields() {
        passwordTextField.text = ""
        emailTextField.text = ""
    }
    
    // MARK: - Actions
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
}

// MARK: - CustomButtonDelegate
extension SignUpViewController: CustomButtonDelegate {
    func customButtonPressed(with id: Int) {
        presenter?.signUpButtonPressed(email: emailTextField.text, password: passwordTextField.text)
    }
}

// MARK: - SignUpViewControllerProtocol
extension SignUpViewController: SignUpViewControllerProtocol {
    func showAlert(with message: String?) {
        let alert = UIAlertController(title: "Error", message: message ?? "Unknown error", preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
