//
//  InspectionQuestionsViewController.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import UIKit
import SnapKit

protocol InspectionQuestionsViewControllerProtocol: AnyObject {
}

final class InspectionQuestionsViewController: UIViewController {
    
    // MARK: Properties
    var presenter: InspectionQuestionsPresenterProtocol?
    private var inspections: Inspections?
    
    // MARK: - GUI variables
    private lazy var tableView: UITableView = {
        let item = UITableView()
        item.backgroundColor = UIColor(named: "backgroundColor")
        item.showsVerticalScrollIndicator = false
        item.register(InspectionsQuestionCell.self, forCellReuseIdentifier: InspectionsQuestionCell.reuseIdentifier)
        item.separatorStyle = .none
        item.delegate = self
        item.dataSource = self
        return item
    }()
    
    // MARK: - Initialization
    func set(presenter: InspectionQuestionsPresenterProtocol, inspections: Inspections) {
        self.presenter = presenter
        self.inspections = inspections
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            presenter?.saveInspections(inspections)
        }
    }
    
    // MARK: - Private methods
    private func configureUI() {
        view.backgroundColor = UIColor(named: "backgroundColor")
        title = "Inspection"
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}

// MARK: - InspectionQuestionsViewControllerProtocol
extension InspectionQuestionsViewController: InspectionQuestionsViewControllerProtocol {
}

// MARK: - UITableViewDelegates
extension InspectionQuestionsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let categories = inspections?.inspection.survey.categories else { return 0 }
        return categories[section].questions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let categories = inspections?.inspection.survey.categories else { return 0 }
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let categories = inspections?.inspection.survey.categories else { return "" }
        return categories[section].name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: InspectionsQuestionCell.reuseIdentifier, for: indexPath) as? InspectionsQuestionCell else { return UITableViewCell() }
        guard let inspection = inspections?.inspection else { return cell }
        
        cell.configureCell(question:  inspection.survey.categories[indexPath.section].questions[indexPath.row].name, answers: inspection.survey.categories[indexPath.row].questions[indexPath.row].answerChoices, selectedId: inspection.survey.categories[indexPath.section].questions[indexPath.row].selectedAnserChoiceId,
                           section: indexPath.section,
                           row: indexPath.row)
        cell.answerDelegate = self
        return cell
    }
}

// MARK: - InspectionQuestionsViewController
extension InspectionQuestionsViewController: InspectionsQuestionCellDelegate {
    func answerTapped(isEmpty: Bool, with id: Int, from section: Int, from row: Int) {
        guard var inspection = inspections?.inspection else { return }
        if isEmpty {
            inspection.survey.categories[section].questions[row].selectedAnserChoiceId = nil
        } else {
            inspection.survey.categories[section].questions[row].selectedAnserChoiceId = id
        }
        self.inspections?.inspection = inspection
    }
}
