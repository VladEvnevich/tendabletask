//
//  InspectionQuestionsPresenterViewController.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import UIKit

protocol InspectionQuestionsPresenterProtocol {
    func saveInspections(_ inspections: Inspections?)
}

final class InspectionQuestionsPresenter {

    // MARK: Properties
    weak private var view: InspectionQuestionsViewControllerProtocol?
    private var coordinator: ProjectCoordinator
    private var realmManager: RealmManagerProtocol
    
    // MARK: Initialization
    init(coordinator: ProjectCoordinator,
         view: InspectionQuestionsViewControllerProtocol,
         realmManager: RealmManagerProtocol) {
        self.coordinator = coordinator
        self.view = view
        self.realmManager = realmManager
    }
}

// MARK: InspectionQuestionsPresenterProtocol
extension InspectionQuestionsPresenter: InspectionQuestionsPresenterProtocol {
    func saveInspections(_ inspections: Inspections?) {
        guard let inspections = inspections else {
            return
        }
        realmManager.saveInspection(inspection: inspections)
    }
}

