//
//  SceneDelegate.swift
//  Tendable
//
//  Created by Vladislav Evnevich on 11.04.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)

        let rootNavigationController = UINavigationController()
        let dependencies = Dependencies()
        let container = DependencyFactory(dependecies: dependencies)
        let coordinator = container.makeInitialCoordinator()

        coordinator.start(rootNavigationController)

        window?.rootViewController = rootNavigationController
        window?.makeKeyAndVisible()
    }
}

