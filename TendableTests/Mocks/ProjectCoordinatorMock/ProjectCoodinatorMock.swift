//
//  ProjectCoodinatorMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

class ProjectCoordinatorMock: RootCoordinator {
    var lastScreenCalled: ScreenCalled = .none
    
    func start(_ navigationController: UINavigationController) {
        // no up
    }
    
    func moveToLogIn() {
        lastScreenCalled = .loginScreen
    }
    
    func moveToListOfInspections() {
        lastScreenCalled = .listOfInspectionsScreen
    }
    
    func moveToInspectionQuestions(with inspections: Inspections) {
        lastScreenCalled = .inspectionQuestionsScreen
    }
    
    func moveToSignUp() {
        lastScreenCalled = .signUpScreen
    }
    
    enum ScreenCalled {
        case loginScreen
        case signUpScreen
        case inspectionQuestionsScreen
        case listOfInspectionsScreen
        case none
    }
}
