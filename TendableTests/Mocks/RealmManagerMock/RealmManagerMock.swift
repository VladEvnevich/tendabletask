//
//  RealmManagerMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

class RealmManagerMock: RealmManagerProtocol {
    func saveInspection(inspection: Inspections) {
        // no up
    }
    
    func getCurrentInspection() -> Inspections? {
        return nil
    }
    
    func removeInspection() {
        // no up
    }
}
