//
//  AuthenticationManagerMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class AuthenticationManagerMock: AuthenticationManagerProtocol {
    var loginError: NetworkError? = nil
    var registrationError: NetworkError? = nil
    
    func login(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void)) {
        if let error = loginError {
            complition(.failure(error))
        } else {
            complition(.success(()))
        }
    }
    
    func registration(email: String, password: String, complition: @escaping ((Result<Void, Error>) -> Void)) {
        if let error = registrationError {
            complition(.failure(error))
        } else {
            complition(.success(()))
        }
    }
}
