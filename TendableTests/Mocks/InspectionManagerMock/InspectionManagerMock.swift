//
//  InspectionManagerMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class InspectionManagerMock: InspectionManagerProtocol {
    var getInspectionError: NetworkError? = nil
    var sendInspectionError: NetworkError? = nil
    
    func getInspection(complition: @escaping ((Result<Inspections, Error>) -> Void)) {
        if let error = getInspectionError {
            complition(.failure(error))
        } else {
            if let inspectionRequestModel = inspectionRequestModel {
                complition(.success((inspectionRequestModel)))
            } else {
                complition(.failure(NetworkError.clientError))
            }
        }
    }
    
    func sendInspection(request: Inspections, complition: @escaping ((Result<Void, Error>) -> Void)) {
        if let error = sendInspectionError {
            complition(.failure(error))
        } else {
            complition(.success(()))
        }
    }
    
    private var inspectionRequestData: Data {
        let testBundle = Bundle(for: InspectionManagerMock.self)
        guard let url = testBundle.url(forResource: "inspection_json_success",
                                       withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
                  return Data()
              }
        return data
    }
    
    private var inspectionRequestModel: Inspections? {
        let decoder = JSONDecoder()
        guard let model = try? decoder.decode(Inspections.self, from: inspectionRequestData) else {
            return nil
        }
        return model
    }
}
