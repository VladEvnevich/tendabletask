//
//  AuthenticationManager.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import XCTest
@testable import Tendable

final class AuthenticationManagerTests: XCTestCase {
    
    var authenticationManager: AuthenticationManagerProtocol?
    var networkClient: NetworkClientProtocol?
    let session = MockURLSession()
    
    override func setUp() {
        super.setUp()
        authenticationManager = AuthenticationManager(networkClient: NetworkClient(session: session))
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Login request tests
    
    func testLoginRequestSuccess() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        
        var successWasCalled = false
        
        authenticationManager?.login(email: "test1", password: "test1", complition: { result in
            switch result {
            case .success():
                successWasCalled = true
            case .failure(let error):
                XCTAssertNotNil(error, "Inspection should not be nil")
            }
            XCTAssert(successWasCalled, "Expected success result")
        })
    }

    func testLoginRequestFailed() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        session.nextStatusCode = StatusCodes.badRequest

        authenticationManager?.login(email: "test2", password: "test2", complition: { result in
            switch result {
            case .success():
                XCTFail("We should not get success result")
            case .failure(let error):
                XCTAssertNotNil(error, "Error should not be nil")
            }
        })
    }
    
    // MARK: Registration request tests
    
    func testRegistrationRequestSuccess() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        
        var successWasCalled = false
        
        authenticationManager?.registration(email: "test1", password: "test1", complition: { result in
            switch result {
            case .success():
                successWasCalled = true
            case .failure(let error):
                XCTAssertNotNil(error, "Inspection should not be nil")
            }
            XCTAssert(successWasCalled, "Expected success result")
        })
    }

    func testRegistrationRequestFailed() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        session.nextStatusCode = StatusCodes.badRequest

        authenticationManager?.registration(email: "test2", password: "test2", complition: { result in
            switch result {
            case .success():
                XCTFail("We should not get success result")
            case .failure(let error):
                XCTAssertNotNil(error, "Error should not be nil")
            }
        })
    }
    
    private struct StatusCodes {
        static let badRequest: Int = 404
    }
}
