//
//  InspectionManagerTests.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import XCTest
@testable import Tendable

final class InspectionManagerTests: XCTestCase {
    
    var inspectionManager: InspectionManagerProtocol?
    var networkClient: NetworkClientProtocol?
    let session = MockURLSession()
    
    override func setUp() {
        super.setUp()
        inspectionManager = InspectionManager(networkClient: NetworkClient(session: session))
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Get inspection tests
    
    func testGetInspectionRequestSuccess() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = inspectionRequestData
        
        inspectionManager?.getInspection(complition: { result in
            switch result {
            case .success(let inspection):
                XCTAssertNotNil(inspection, "Inspection should not be nil")
            case .failure(let error):
                XCTFail("We should not get error \(error)")
            }
        })
    }
    
    func testGetInspectionRequestFailed() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        session.nextStatusCode = StatusCodes.badRequest
        
        inspectionManager?.getInspection(complition: { result in
            switch result {
            case .success(let inspection):
                XCTFail("We should not get success with model \(inspection)")
            case .failure(let error):
                XCTAssertNotNil(error, "Error should not be nil")
            }
        })
    }
    
    // MARK: Send inspection tests
    
    func testSendInspectionRequestSuccess() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        
        guard let inspections = inspectionRequestModel else {
            XCTFail("Request should not be nil")
            return
        }
        inspectionManager?.sendInspection(request: inspections, complition: { result in
            switch result {
            case .success(let inspection):
                XCTAssertNotNil(inspection, "Inspection should not be nil")
            case .failure(let error):
                XCTFail("We should not get error \(error)")
            }
        })
    }
    
    func testSendInspectionRequestFailed() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        session.nextData = nil
        session.nextStatusCode = StatusCodes.badRequest
        
        guard let inspections = inspectionRequestModel else {
            XCTFail("Request should not be nil")
            return
        }
        inspectionManager?.sendInspection(request: inspections, complition: { result in
            switch result {
            case .success():
                XCTFail("We should not get success")
            case .failure(let error):
                XCTAssertNotNil(error, "Error should not be nil")
            }
        })
    }
    
    
    // MARK: Helpers
    
    private var inspectionRequestData: Data {
        let testBundle = Bundle(for: NetworkClientTests.self)
        guard let url = testBundle.url(forResource: "inspection_json_success",
                                       withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
                  return Data()
              }
        return data
    }
    
    private var inspectionRequestModel: Inspections? {
        let decoder = JSONDecoder()
        guard let model = try? decoder.decode(Inspections.self, from: inspectionRequestData) else {
            return nil
        }
        return model
    }
    
    private struct StatusCodes {
        static let badRequest: Int = 404
    }
}
