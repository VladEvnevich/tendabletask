//
//  ListOfInspectionsPresenterTests.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class ListOfInspectionsPresenterTests: XCTestCase {
    
    var presenter: ListOfInspectionsPresenterProtocol?
    let inspectionManager = InspectionManagerMock()
    let view = ListOfInspectionsViewMock()
    let coordinator = ProjectCoordinatorMock()
    
    override func setUp() {
        super.setUp()
        let userDefaults: UserDefaultsManagerProtocol = UserDefaultsManager()
        let realmManager: RealmManagerProtocol = RealmManagerMock()
        presenter = ListOfInspectionsPresenter(coordinator: coordinator, view: view, inspectionManager: inspectionManager, userDefaultsManager: userDefaults, realmManager: realmManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetInspectionFailed() {
        inspectionManager.getInspectionError = .clientError
        presenter?.fetchRemoteInspections()
        
        XCTAssertNotNil(view.messageAlert)
    }
    
    func testGetInspectionSuccess() {
        inspectionManager.getInspectionError = nil
        presenter?.fetchRemoteInspections()
        
        XCTAssertNil(view.messageAlert)
        XCTAssertNotNil(view.inspections)
    }
    
    func testGetLocalEmptyInspectionFailed() {
        inspectionManager.getInspectionError = .clientError
        presenter?.fetchLocalInspections()
        
        XCTAssertTrue(view.initialStateWasCalled)
    }
    
    func testLogout() {
        presenter?.logoutButtonDidTap()
        
        XCTAssertTrue(coordinator.lastScreenCalled == .signUpScreen)
    }
    
    func testContinue() {
        view.inspections = inspectionRequestModel
        presenter?.continueButtonDidTap()
        
        XCTAssertTrue(coordinator.lastScreenCalled == .inspectionQuestionsScreen)
    }
    
    private var inspectionRequestData: Data {
        let testBundle = Bundle(for: ListOfInspectionsPresenterTests.self)
        guard let url = testBundle.url(forResource: "inspection_json_success",
                                       withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
                  return Data()
              }
        return data
    }
    
    private var inspectionRequestModel: Inspections? {
        let decoder = JSONDecoder()
        guard let model = try? decoder.decode(Inspections.self, from: inspectionRequestData) else {
            return nil
        }
        return model
    }
}
