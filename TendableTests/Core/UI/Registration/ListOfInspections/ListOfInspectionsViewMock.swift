//
//  ListOfInspectionsViewMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class ListOfInspectionsViewMock: ListOfInspectionsViewControllerProtocol {
    var inspections: Inspections?
    
    var messageAlert: String?
    var initialStateWasCalled: Bool = false
    
    func showAlert(title: String, message: String, style: UIAlertAction.Style) {
        self.messageAlert = message
    }
    
    func showInspections(_ inspections: Inspections) {
        self.inspections = inspections
    }
    
    func showInitialState() {
        self.initialStateWasCalled = true
    }
}
