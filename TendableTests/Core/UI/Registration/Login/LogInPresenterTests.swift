//
//  LogInPresenterTests.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class LogInPresenterTests: XCTestCase {
    var presenter: LogInPresenterProtocol?
    let authentificationManager = AuthenticationManagerMock()
    let coordinator = ProjectCoordinatorMock()
    let view = LogInViewMock()
    
    override func setUp() {
        super.setUp()
        
        let userDefaults = UserDefaultsManager()
        presenter = LogInPresenter(coordinator: coordinator, view: view, authenticationManager: authentificationManager, userDefaultsManager: userDefaults)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoginFailed() {
        authentificationManager.loginError = .clientError
        presenter?.logInButtonPressed(email: "test1", password: "test1")
        
        XCTAssertNotNil(view.messageAlert)
    }
    
    func testLoginSuccess() {
        authentificationManager.loginError = nil
        presenter?.logInButtonPressed(email: "test1", password: "test1")
        
        XCTAssertNil(view.messageAlert)
        XCTAssertTrue(coordinator.lastScreenCalled == .listOfInspectionsScreen)
    }
    
    func testLoginFieldsIsEmpty() {
        authentificationManager.loginError = nil
        presenter?.logInButtonPressed(email: "", password: "")
        
        XCTAssertNotNil(view.messageAlert)
    }
}
