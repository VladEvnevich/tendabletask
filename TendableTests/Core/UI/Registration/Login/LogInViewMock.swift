//
//  LogInViewMock.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class LogInViewMock: LogInViewControllerProtocol {
    var messageAlert: String?
    func showAlert(with message: String?) {
        messageAlert = message
    }
}
