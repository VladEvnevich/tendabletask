//
//  SignUpPresenterTests.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 13.04.2022.
//

import XCTest
@testable import Tendable

final class SignUpPresenterTests: XCTestCase {
    
    var presenter: SignUpPresenterProtocol?
    let authentificationManager = AuthenticationManagerMock()
    let view = SignUpViewMock()
    let coordinator = ProjectCoordinatorMock()
    
    override func setUp() {
        super.setUp()
        presenter = SignUpPresenter(coordinator: coordinator, view: view, authenticationManager: authentificationManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSignUpFailed() {
        authentificationManager.registrationError = .clientError
        presenter?.signUpButtonPressed(email: "test1", password: "test1")
        
        XCTAssertNotNil(view.messageAlert)
    }
    
    func testSignUpSuccess() {
        authentificationManager.registrationError = nil
        presenter?.signUpButtonPressed(email: "test1", password: "test1")
        
        XCTAssertTrue(coordinator.lastScreenCalled == .loginScreen)
        XCTAssertNil(view.messageAlert)
    }
    
    func testSignUpFieldsIsEmpty() {
        authentificationManager.registrationError = nil
        presenter?.signUpButtonPressed(email: "", password: "")
        
        XCTAssertNotNil(view.messageAlert)
    }
}
