//
//  MockURLSessionDataTask.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import Foundation
import XCTest
@testable import Tendable

final class MockURLSessionDataTask: URLSessionDataTaskProtocol {
    private (set) var resumeWasCalled = false
    
    func resume() {
        resumeWasCalled = true
    }
}
