//
//  NetworkClientTests.swift
//  TendableTests
//
//  Created by Vladislav Evnevich on 12.04.2022.
//

import XCTest
@testable import Tendable

final class NetworkClientTests: XCTestCase {
    
    var networkClient: NetworkClientProtocol?
    let session = MockURLSession()
    
    override func setUp() {
        super.setUp()
        networkClient = NetworkClient(session: session)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetRequest() {

        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.getInspections)
        
        networkClient?.callAPI(request: request) { (response: Result<Inspection, Error>) in
            // Return data
        }
        
        XCTAssert(session.lastURL == request.url)
        
    }
    
    func testGetResumeCalled() {
        
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.getInspections)
        
        networkClient?.callAPI(request: request) { (response: Result<Inspection, Error>) in
            // Return data
        }
        
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func testGetShouldReturnData() {
        let expectedData = self.inspectionResponseBody
        let request = NetworkRouter.configureURLRequest(with: MainEndpoint.getInspections)
        
        session.nextData = expectedData
        
        var actualData: Inspections?
        networkClient?.callAPI(request: request) { (response: Result<Inspections, Error>) in
            switch response {
            case .success(let model):
                actualData = model
            case .failure(_):
                XCTFail("Should't be error")
            }
        }
        
        XCTAssertNotNil(actualData)
    }
    
    // MARK: Helpers
    
    private var inspectionResponseBody: Data {
        let testBundle = Bundle(for: NetworkClientTests.self)
        guard let url = testBundle.url(forResource: "inspection_json_success",
                                       withExtension: "json"),
              let data = try? Data(contentsOf: url) else {
                  return Data()
              }
        return data
    }
}
